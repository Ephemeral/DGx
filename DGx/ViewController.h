//
//  ViewController.h
//  DGx
//
//  Created by Ephemeral on 20.07.17.
//  Copyright © 2017 Ephemeral. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <CoreFoundation/CoreFoundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <ImageIO/ImageIO.h>
#import <CoreServices/CoreServices.h>

@interface ViewController : NSViewController

@end

