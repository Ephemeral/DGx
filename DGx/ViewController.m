//
//  ViewController.m
//  DGx
//
//  Created by Ephemeral on 20.07.17.
//  Copyright © 2017 Ephemeral. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.window.opaque = false;
    self.view.window.backgroundColor = NSColor.clearColor;
}
- (void)viewDidLayout
{
    [self.view.window setLevel: NSStatusWindowLevel];
}


-(void)onTick:(NSTimer *)timer
{

}

- (void)setRepresentedObject:(id)representedObject
{
    [super setRepresentedObject:representedObject];


}


- (BOOL)canBecomeKeyWindow
{
    
    return YES;
}



NSPoint initialLocation = {0};
- (void)mouseDown:(NSEvent *)Event
{

    initialLocation = [Event locationInWindow];
}

- (void)mouseDragged:(NSEvent *)Event
{
    
    NSRect ViewableFrame = [[NSScreen mainScreen] visibleFrame];
    NSRect WindowDimensions = [self.view.window frame];
    NSPoint Delta = WindowDimensions.origin;
    

    NSPoint currentLocation = [Event locationInWindow];

    
    Delta.x += (currentLocation.x - initialLocation.x);
    Delta.y += (currentLocation.y - initialLocation.y);
    
    
    if ((Delta.y + WindowDimensions.size.height) > (ViewableFrame.origin.y + ViewableFrame.size.height))
    {
        Delta.y = ViewableFrame.origin.y + (ViewableFrame.size.height - WindowDimensions.size.height);
    }
    

    [self.view.window setFrameOrigin:Delta];
     
}



NSEvent* EventHandle = nil;

- (IBAction)Configure:(id)sender
{

    
    EventHandle = [NSEvent addGlobalMonitorForEventsMatchingMask:NSEventMaskLeftMouseDown handler:^(NSEvent* Event)
     {
         
         BOOL Found = NO;
         
         unsigned long WindowId = kCGNullWindowID;
         
         
         CFArrayRef WindowsList = CGWindowListCopyWindowInfo(kCGWindowListOptionAll | kCGWindowListOptionOnScreenOnly, kCGNullWindowID);
         
         
         CFIndex Windowindices = CFArrayGetCount(WindowsList);

         
         for (int32_t CurrentWindow = 0; CurrentWindow < Windowindices ; ++CurrentWindow)
         {
             
             CFDictionaryRef WindowInfo = (CFDictionaryRef)CFArrayGetValueAtIndex(WindowsList, CurrentWindow);
             
             CFNumberRef CurrentLayer = (CFNumberRef)CFDictionaryGetValue(WindowInfo, kCGWindowLayer);
             
             long CurrentLayerVal = LONG_MAX;
             
             CFNumberGetValue(CurrentLayer, kCFNumberLongType, &CurrentLayerVal);
             
             if (CurrentLayerVal > 0)
                 continue;
             
             
             CFStringRef WindowTitle = (CFStringRef)CFDictionaryGetValue(WindowInfo, kCGWindowName);
             
             CFStringRef Target = CFStringCreateWithCString(kCFAllocatorDefault, "Runescape", kCFStringEncodingUTF8);

             
             if(CFStringFind(WindowTitle, Target, kCFCompareCaseInsensitive | kCFCompareWidthInsensitive).location != kCFNotFound)
                 Found = YES;
             
             CFRelease(Target);
             
             CFRelease(WindowTitle);
             
             CFNumberRef CurrentWindowNumber = (CFNumberRef)CFDictionaryGetValue(WindowInfo, kCGWindowNumber);
             
             CFNumberGetValue(CurrentWindowNumber, kCFNumberLongType, &WindowId);
             
             
             CFDictionaryRef WindowBounds = (CFDictionaryRef)CFDictionaryGetValue(WindowInfo, kCGWindowBounds);
             
             CGRect WindowRect = {0};
             
             
             CGRectMakeWithDictionaryRepresentation(WindowBounds, &WindowRect);

             

             
             if(Found)
             {
                
                 
                 break;
             }
             
         }

         [NSEvent removeMonitor:EventHandle];
        
     }];
    
}


@end
